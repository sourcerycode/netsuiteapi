function main(datain) {

    if (datain.ver != "1.0")
        return { success: false, error: "outdated restlet" };

    try {

        if (datain.log)
            nlapiLogExecution('debug', datain.log, JSON.stringify(datain));

        if (datain.op == "search") {

            var columns = [];

            if (datain.columns)
                datain.columns.forEach(function (c) {

                    var col = new nlobjSearchColumn(c.name, c.join, c.summary);

                    if (c.sort)
                        if (c.sort == "desc")
                            col = col.setSort(true);
                        else if (c.sort == "asc")
                            col = col.setSort();

                    if (c.formula)
                        col = col.setFormula(c.formula);

                    if (c.function)
                        col = col.setFunction(c.function);

                    if (c.label)
                        col = col.setLabel(c.label);

                    columns.push(col);

                });

            var filters = datain.filters;
            if (datain.searchFilters) {
                filters = [];
                datain.searchFilters.forEach(function (f) {

                    filters.push(new nlobjSearchFilter(f.name, f.join, f.operator, f.value1, f.value2));

                });
            }

            var searchResult = nlapiSearchRecord(datain.type, datain.id, filters, columns);

            return {
                success: true,
                result: searchResult == null ? [] : searchResult
            };

        }
        else if (datain.op == "savedsearch") {

            var savedSearch = nlapiLoadSearch(datain.type, datain.id);

            if (datain.filters)
                savedSearch.setFilterExpression(datain.filters);

            var resultSet = savedSearch.runSearch();
            var searchResult = [];
            var id = 0;
            do {
                var resultSlice = resultSet.getResults(id, id + 1000);
                for (var rs in resultSlice) {
                    searchResult.push(resultSlice[rs]);
                    id++;
                }
            } while (resultSlice.length >= 1000);

            return {
                success: true,
                result: searchResult == null ? [] : searchResult
            };

        }
        else if (datain.op == "savedsearchpaged") {

            var savedSearch = nlapiLoadSearch(datain.type, datain.id);
            var resultSet = savedSearch.runSearch();

            var searchResult = resultSet.getResults(datain.resultIndex, datain.resultIndex + datain.resultCount);

            return {
                success: true,
                result: searchResult == null ? [] : searchResult
            };

        }
        else if (datain.op == "submitfields") {

            var errors = [];
            var successes = [];
            var failures = [];

            datain.records.forEach(function (record) {

                try {

                    var fields = [];
                    var values = [];

                    record.fields.list.forEach(function (x) {
                        fields.push(x.name);
                        values.push(x.value);
                    });

                    nlapiSubmitField(record.type, record.id, fields, values, record.doSourcing);
                    successes.push(record.id);

                } catch (e) {

                    failures.push(record.id);
                    errors.push(e);

                }

            });

            return {
                success: failures.length == 0,
                result: {
                    successes: successes,
                    failures: failures,
                    errors: errors
                }
            };

        }
        else if (datain.op == "submitrecords") {

            var errors = [];
            var successes = [];
            var failures = [];

            datain.records.forEach(function (record) {

                try {

                    var recordToSubmit = nlapiLoadRecord(record.type, record.id);

                    if (record.fields)
                        record.fields.list.forEach(function (x) {

                            recordToSubmit.setFieldValue(x.name, x.value);

                        });

                    if (record.linesToUpdate)
                        record.linesToUpdate.forEach(function (line) {

                            line.fields.list.forEach(function (field) {

                                recordToSubmit.setLineItemValue(line.group, field.name, line.linenum, field.value);

                            });

                        });

                    if (record.linesToCreate)
                        record.linesToCreate.forEach(function (line) {

                            recordToSubmit.selectNewLineItem(line.group);

                            line.fields.list.forEach(function (field) {

                                recordToSubmit.setCurrentLineItemValue(line.group, field.name, field.value);

                            });

                            recordToSubmit.commitLineItem(line.group);

                        });

                    var id = nlapiSubmitRecord(recordToSubmit, record.doSourcing, record.ignoreMandatoryFields);
                    successes.push(id);

                } catch (e) {

                    failures.push(record.id);
                    errors.push(e);

                }

            });

            return {
                success: failures.length == 0,
                result: {
                    successes: successes,
                    failures: failures,
                    errors: errors
                }
            };

        }
        else if (datain.op == "loadrecord") {

            var record = nlapiLoadRecord(datain.type, datain.id);

            var fields = [];
            record.getAllFields().forEach(function (f) {

                var field = record.getField(f);

                if (field)
                    fields.push({
                        label: field.getLabel(),
                        type: field.getType(),
                        name: field.getName()
                    });

            });

            return {
                success: true,
                result: {
                    record: record,
                    fields: fields
                }
            };

        }
        else if (datain.op == "transformrecords") {

            var errors = [];
            var successes = [];
            var failures = [];

            datain.records.forEach(function (record) {

                try {

                    var transformedRecord = nlapiTransformRecord(record.type, record.id, record.transformType, record.transformValues);
                    var id = nlapiSubmitRecord(transformedRecord, record.doSourcing, record.ignoreMandatoryFields);

                    successes.push({ id: record.id, transformedId: id });

                } catch (e) {

                    failures.push(record.id);
                    errors.push(e);

                }

            });

            return {
                success: failures.length == 0,
                result: {
                    successes: successes,
                    failures: failures,
                    errors: errors
                }
            };

        }
        else if (datain.op == "createrecord") {

            var record = nlapiCreateRecord(datain.type);

            if (datain.fields)
                datain.fields.list.forEach(function (x) {

                    record.setFieldValue(x.name, x.value);

                });

            if (datain.lines)
                datain.lines.forEach(function (line) {

                    record.selectNewLineItem(line.group);

                    line.fields.list.forEach(function (field) {

                        record.setCurrentLineItemValue(line.group, field.name, field.value);

                    });

                    record.commitLineItem(line.group);

                });

            return {
                success: true,
                result: nlapiSubmitRecord(record, datain.doSourcing, datain.ignoreMandatoryFields)
            };

        }
        else if (datain.op == "deleterecords") {

            var errors = [];
            var successes = [];
            var failures = [];

            datain.records.forEach(function (record) {

                try {

                    var id = nlapiDeleteRecord(record.type, record.id);
                    successes.push(id);

                } catch (e) {

                    failures.push(record.id);
                    errors.push(e);

                }

            });

            return {
                success: failures.length == 0,
                result: {
                    successes: successes,
                    failures: failures,
                    errors: errors
                }
            };

        }
        else if (datain.op == "removelineitems") {

            var record = nlapiLoadRecord(datain.type, datain.id);
            
            if (datain.lineNum) {
                record.removeLineItem(datain.group, datain.lineNum);
            }
            else
            {
                var lineCount = record.getLineItemCount(datain.group);
                for (var i = lineCount; i >= 1; i--) {
                    record.removeLineItem(datain.group, i);
                }
            }
                
            nlapiSubmitRecord(record, true, true);

            return {
                success: true,
                result: {
                    record: record
                }
            };

        }
        else if (datain.op == "loadfile") {

            var file = nlapiLoadFile(datain.id);

            return {
                success: true,
                result: {
                    description: file.getDescription(),
                    folder: file.getFolder(),
                    id: file.getId(),
                    name: file.getName(),
                    size: file.getSize(),
                    type: file.getType(),
                    url: file.getURL(),
                    value: datain.getValue ? file.getValue() : null,
                    isInactive: file.isInactive(),
                    isOnline: file.isOnline(),
                }
            };

        }
        else if (datain.op == "submitfile") {

            var file = nlapiLoadFile(datain.id);

            if (datain.description)
                file.setDescription(datain.description);

            if (datain.encodingType)
                file.setEncoding(datain.encodingType);

            if (datain.folderId)
                file.setFolder(datain.folderId);

            if (datain.isInactive != null)
                file.setIsInactive(datain.isInactive);

            if (datain.isOnline != null)
                file.setIsOnline(datain.isOnline);

            if (datain.name)
                file.setName(datain.name);

            nlapiSubmitFile(file);

            return {
                success: true
            };

        }
        else if (datain.op == "deletefile") {

            nlapiDeleteFile(datain.id);
            
            return {
                success: true
            };

        }
        else if (datain.op == "createfile") {

            var file = nlapiCreateFile(datain.name, datain.type, datain.contents);
            file.setFolder(datain.folderId);
            var fileId = nlapiSubmitFile(file);

            return {
                success: true,
                result: {
                    id: fileId
                }
            };

        }
        else if (datain.op == "printrecord") {

            var file = nlapiPrintRecord(datain.type, datain.id, datain.mode, datain.properties);

            return {
                success: true,
                result: {
                    description: file.getDescription(),
                    folder: file.getFolder(),
                    id: file.getId(),
                    name: file.getName(),
                    size: file.getSize(),
                    type: file.getType(),
                    url: file.getURL(),
                    value: datain.getValue ? file.getValue() : null,
                    isInactive: file.isInactive(),
                    isOnline: file.isOnline()
                }
            };
   
        } else if (datain.op == "printrecordemail") {

            var file = nlapiPrintRecord(datain.type, datain.id, datain.mode, datain.properties);

            nlapiSendEmail(datain.author, datain.recipient, datain.subject, datain.body, datain.cc, datain.bcc, datain.records, file);

            return {
                success: true
            };

        } else if (datain.op == "sendemail") {

            nlapiSendEmail(datain.author, datain.recipient, datain.subject, datain.body, datain.cc, datain.bcc, datain.records);

            return {
                success: true
            };

        }

        return { success: false, error: "bad operation" };

    } catch (error) {

        return { success: false, error: error };

    }

}