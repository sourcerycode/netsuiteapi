﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace NetsuiteAPI
{
    public class OAuth
    {

        public static string GeneratHeader(NetsuiteTokenAuth auth, Uri uri, string method)
        {

            const string signatureMethod = "HMAC-SHA256";
            string nonce = Guid.NewGuid().ToString("n");
            string timeStamp = ((long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds).ToString();
            string baseURL = uri.GetLeftPart(UriPartial.Path);
            string baseUrlEncoded = UrlEncode(baseURL);

            NameValueCollection query = HttpUtility.ParseQueryString(uri.Query);
            query.Add("oauth_consumer_key", auth.ConsumerKey);
            query.Add("oauth_nonce", nonce);
            query.Add("oauth_signature_method", signatureMethod);
            query.Add("oauth_timestamp", timeStamp);
            query.Add("oauth_token", auth.TokenID);
            query.Add("oauth_version", "1.0");

            string baseStr = string.Join("&", query.AllKeys.OrderBy(i => i).Select(i => $"{i}={query[i]}"));
            string baseStrUrlEncoded = UrlEncode(baseStr);

            byte[] key = Encoding.ASCII.GetBytes($"{auth.ConsumerSecret}&{auth.TokenSecret}");
            byte[] signature;
            using (HMACSHA256 hmac = new HMACSHA256(key))
                signature = hmac.ComputeHash(Encoding.ASCII.GetBytes($"{method}&{baseUrlEncoded}&{baseStrUrlEncoded}"));

            string signatureUrlEncoded = UrlEncode(Convert.ToBase64String(signature));

            var oauth_params = new List<KeyValuePair<string, string>>();
            oauth_params.Add(new KeyValuePair<string, string>("realm", auth.AccountID));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_consumer_key", auth.ConsumerKey));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_token", auth.TokenID));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_nonce", nonce));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_timestamp", timeStamp));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_signature_method", signatureMethod));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_version", "1.0"));
            oauth_params.Add(new KeyValuePair<string, string>("oauth_signature", signatureUrlEncoded));

            string oauth = string.Join(",", oauth_params.Select(i => $"{i.Key}=\"{i.Value}\""));

            return $"OAuth {oauth}";

        }

        private static string UrlEncode(string value)
        {

            return Regex.Replace(HttpUtility.UrlEncode(value), @"%[a-f\d]{2}", m => m.Value.ToUpper());

        }

    }
}
