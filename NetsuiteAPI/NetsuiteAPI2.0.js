﻿require(["N/search"], function (search) {

    var r = search.create({

        type: "salesorder",
        columns: ["trandate", "entity"],
        filters: [["mainline", "is", "T"], "and", ["tranid", "is", "SO2098390"]]

    }).run();

    var rr = r.getRange({ start: 0, end: 1000 });

    log(r);

});