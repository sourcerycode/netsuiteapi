﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace NetsuiteAPI
{
    public class NetsuiteClient : IDisposable
    {

        NetsuiteTokenAuth auth;
        HttpClient client;

        private const string restletVersion = "1.0";

        public Dictionary<string, (DateTime, object)> cache = new Dictionary<string, (DateTime, object)>();

        public NetsuiteClient(NetsuiteTokenAuth auth, int timeoutSeconds = 100)
        {

            this.auth = auth;

            client = new HttpClient() { Timeout = TimeSpan.FromSeconds(timeoutSeconds) };

        }

        public void Dispose()
        {
            client.Dispose();
        }

        public Result<T> CallRestlet<T>(object data)
        {

            return CallRestlet<T>(auth.RestletUrl, data);

        }

        public Result<T> CallRestlet<T>(Uri uri, object data)
        {
            string requestJson = JsonConvert.SerializeObject(data);

            var response = client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = uri,
                Headers =
                {
                    { "Authorization", OAuth.GeneratHeader(auth, uri, HttpMethod.Post.Method) }
                },
                Content = new StringContent(requestJson, Encoding.UTF8, "application/json")

            }).Result;

            //response.EnsureSuccessStatusCode();

            string responseJson = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<Result<T>>(responseJson);

        }

        #region Search

        public Result<T> SavedSearch<T>(string type, string id, IEnumerable<object> filters = null, CacheSettings cacheSettings = null)
        {

            if (cacheSettings != null && cache.ContainsKey(cacheSettings.id) && DateTime.Now.Subtract(cache[cacheSettings.id].Item1) < cacheSettings.life)
                return (Result<T>)cache[cacheSettings.id].Item2;

            var result = CallRestlet<T>(new { ver = restletVersion, op = "savedsearch", type, id, filters });

            if (cacheSettings != null)
                cache[cacheSettings.id] = (DateTime.Now, result);

            return result;

        }

        public Result<T> SavedSearch<T>(string type, string id, int index, int count, CacheSettings cacheSettings = null)
        {

            if (cacheSettings != null && cache.ContainsKey(cacheSettings.id) && DateTime.Now.Subtract(cache[cacheSettings.id].Item1) < cacheSettings.life)
                return (Result<T>)cache[cacheSettings.id].Item2;

            var result = CallRestlet<T>(new { ver = restletVersion, op = "savedsearchpaged", type, id, resultIndex = index, resultCount = count });

            if (cacheSettings != null)
                cache[cacheSettings.id] = (DateTime.Now, result);

            return result;

        }

        public Result<T> Search<T>(string type, string id, CacheSettings cacheSettings = null)
        {

            if (cacheSettings != null && cache.ContainsKey(cacheSettings.id) && DateTime.Now.Subtract(cache[cacheSettings.id].Item1) < cacheSettings.life)
                return (Result<T>)cache[cacheSettings.id].Item2;

            var result = CallRestlet<T>(new { ver = restletVersion, op = "search", type, id });

            if (cacheSettings != null)
                cache[cacheSettings.id] = (DateTime.Now, result);

            return result;

        }

        public Result<T> Search<T>(string type, IEnumerable<object> filters, IEnumerable<string> columns = null, CacheSettings cacheSettings = null)
        {
            return Search<T>(type, filters, columns != null ? columns.Select(x => new SearchColumn { name = x }) : null, cacheSettings);
        }

        public Result<T> Search<T>(string type, IEnumerable<object> filters, IEnumerable<SearchColumn> columns, CacheSettings cacheSettings = null)
        {

            if (cacheSettings != null && cache.ContainsKey(cacheSettings.id) && DateTime.Now.Subtract(cache[cacheSettings.id].Item1) < cacheSettings.life)
                return (Result<T>)cache[cacheSettings.id].Item2;

            var result = CallRestlet<T>(new { ver = restletVersion, op = "search", type, filters, columns });

            if (cacheSettings != null)
                cache[cacheSettings.id] = (DateTime.Now, result);

            return result;

        }

        public Result<T> Search<T>(string type, IEnumerable<SearchFilter> searchFilters, IEnumerable<SearchColumn> columns, CacheSettings cacheSettings = null)
        {

            if (cacheSettings != null && cache.ContainsKey(cacheSettings.id) && DateTime.Now.Subtract(cache[cacheSettings.id].Item1) < cacheSettings.life)
                return (Result<T>)cache[cacheSettings.id].Item2;

            var result = CallRestlet<T>(new { ver = restletVersion, op = "search", type, searchFilters, columns });

            if (cacheSettings != null)
                cache[cacheSettings.id] = (DateTime.Now, result);

            return result;

        }

        #endregion

        #region Submit Fields

        public Result<SubmitResult> SubmitField(
            string type,
            string id,
            string field,
            object value,
            bool doSourcing = false)
        {
            return SubmitFields(new[] {
                new SubmitFieldsRecord {
                    type = type,
                    id = id,
                    fields = new Fields(new[]{ (field, value) }),
                    doSourcing = doSourcing
                }
            });
        }

        public Result<SubmitResult> SubmitFields(
            string type,
            string id,
            Fields fields,
            bool doSourcing = false)
        {
            return SubmitFields(new[] {
                new SubmitFieldsRecord {
                    type = type,
                    id = id,
                    fields = fields,
                    doSourcing = doSourcing
                }
            });
        }

        public Result<SubmitResult> SubmitFields(IEnumerable<SubmitFieldsRecord> records)
        {
            return CallRestlet<SubmitResult>(new
            {
                ver = restletVersion,
                op = "submitfields",
                records
            });
        }

        #endregion

        #region Submit Records

        public Result<SubmitResult> SubmitRecord(
            string type,
            string id,
            Fields fields,
            IEnumerable<ExistingLine> linesToUpdate = null,
            IEnumerable<NewLine> linesToCreate = null,
            bool doSourcing = false,
            bool ignoreMandatoryFields = false)
        {
            return SubmitRecord(new SubmitRecord
            {
                type = type,
                id = id,
                fields = fields,
                linesToUpdate = linesToUpdate,
                linesToCreate = linesToCreate,
                doSourcing = doSourcing,
                ignoreMandatoryFields = ignoreMandatoryFields
            });
        }

        public Result<SubmitResult> SubmitRecord(SubmitRecord record)
        {
            return SubmitRecords(new[] { record });
        }

        public Result<SubmitResult> SubmitRecords(IEnumerable<SubmitRecord> records)
        {
            return CallRestlet<SubmitResult>(new
            {
                ver = restletVersion,
                op = "submitrecords",
                records
            });
        }

        #endregion

        #region Misc Record

        public Result<string> CreateRecord(
            string type,
            Fields fields,
            IEnumerable<NewLine> lines,
            bool doSourcing = false,
            bool ignoreMandatoryFields = false, string log = null)
        {
            return CallRestlet<string>(
                new
                {
                    ver = restletVersion,
                    op = "createrecord",
                    type,
                    fields,
                    lines,
                    doSourcing,
                    ignoreMandatoryFields,
                    log
                }
            );
        }

        public Result<DeleteResult> DeleteRecord(string type, string id)
        {
            return DeleteRecords(new[] { new DeleteRecord { id = id, type = type } });
        }

        public Result<DeleteResult> DeleteRecords(string type, IEnumerable<string> ids)
        {
            return DeleteRecords(ids.Select(x => new DeleteRecord { id = x, type = type }));
        }

        public Result<DeleteResult> DeleteRecords(IEnumerable<DeleteRecord> records)
        {
            return CallRestlet<DeleteResult>(new { ver = restletVersion, op = "deleterecords", records });
        }

        public Result<T> LoadRecord<T>(string type, string id)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "loadrecord", type, id });
        }

        public Result<T> RemoveLineItems<T>(string type, string id, string group)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "removelineitems", type, id, group });
        }

        public Result<T> RemoveLineItem<T>(string type, string id, string group, int lineNum)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "removelineitems", type, id, group, lineNum });
        }

        public Result<TransformResult> TransformRecord(
            string type,
            string id,
            string transformType,
            Fields transformValues = null,
            bool doSourcing = false,
            bool ignoreMandatoryFields = false)
        {

            return TransformRecord(new[] { new TransformRecord {
                type = type,
                id = id,
                transformType = transformType,
                transformValues = transformValues?.list.ToDictionary(x=> x.name, x=>x.value),
                doSourcing = doSourcing,
                ignoreMandatoryFields = ignoreMandatoryFields
            } });
        }

        public Result<TransformResult> TransformRecord(IEnumerable<TransformRecord> records)
        {
            return CallRestlet<TransformResult>(new { ver = restletVersion, op = "transformrecords", records });
        }

        public Result<T> PrintRecord<T>(string type, string id, string mode, object properties = null, bool getValue = false)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "printrecord", getValue, type, id, mode, properties });
        }
        public Result<T> PrintRecordEmail<T>(
            string type, string id, string mode, object properties, 
            string author, string recipient, string subject, string body, string[] cc, string[] bcc, object records)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "printrecordemail", type, id, mode, properties, author, recipient, subject, body, cc, bcc, records });

        }

        public Result<T> SendEmail<T>(string author, string recipient, string subject, string body, string[] cc, string[] bcc, object records)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "sendemail", author, recipient, subject, body, cc, bcc, records });

        }

        #endregion

        #region Files

        public Result<T> LoadFile<T>(string idOrPath, bool getValue = false)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "loadfile", getValue, id = idOrPath });
        }

        public Result<T> DeleteFile<T>(string id)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "deletefile", id });
        }

        public Result<T> SubmitFile<T>(string id, string description = null, string encodingType = null, string folderId = null, bool? isInactive = null, bool? isOnline = null, string name = null)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "submitfile", id, description, encodingType, folderId, isInactive, isOnline, name });
        }

        public Result<T> CreateFile<T>(string name, string type, string contents, string folderId)
        {
            return CallRestlet<T>(new { ver = restletVersion, op = "createfile", name, type, contents, folderId });
        }
        #endregion

        public string FormatDateTime(DateTime dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy hh:mm:ss tt");
        }
        public string FormatDate(DateTime date)
        {
            return date.ToShortDateString();
        }

    }

    public class CacheSettings
    {

        internal string id { get; set; }
        internal TimeSpan life { get; set; }

        public CacheSettings(string id, TimeSpan life)
        {
            this.id = id;
            this.life = life;
        }
    }

    public class DeleteResult
    {

        public string[] successes { get; set; }
        public string[] failures { get; set; }
        public string[] errors { get; set; }

    }

    public class SubmitResult
    {

        public string[] successes { get; set; }
        public string[] failures { get; set; }
        public string[] errors { get; set; }

    }

    public class TransformedRecord
    {
        public string id { get; set; }
        public string transformedId { get; set; }
    }

    public class TransformResult
    {

        public TransformedRecord[] successes { get; set; }
        public string[] failures { get; set; }
        public string[] errors { get; set; }
    }

    public class Result<T>
    {
        public bool success { get; set; }
        public string error
        {
            get; set;
        }
        public T result { get; set; }
    }

    public class SubmitRecord
    {

        public string type { get; set; }
        public string id { get; set; }
        public bool doSourcing { get; set; }
        public bool ignoreMandatoryFields { get; set; }
        public IEnumerable<ExistingLine> linesToUpdate { get; set; }
        public IEnumerable<NewLine> linesToCreate { get; set; }
        public Fields fields { get; set; }

    }

    public class TransformRecord
    {

        public string type { get; set; }
        public string id { get; set; }
        public bool doSourcing { get; set; }
        public bool ignoreMandatoryFields { get; set; }
        public string transformType { get; set; }
        public Dictionary<string, object> transformValues { get; set; }

    }

    public class DeleteRecord
    {

        public string type { get; set; }
        public string id { get; set; }

    }

    public class ExistingLine
    {
        public string group { get; set; }
        public int linenum { get; set; }
        public Fields fields { get; set; }

        public ExistingLine()
        {

        }

        public ExistingLine(string group, int linenum, Fields fields)
        {
            this.group = group;
            this.linenum = linenum;
            this.fields = fields;
        }

    }

    public class NewLine
    {

        public string group { get; set; }
        public Fields fields { get; set; }

        public NewLine()
        {

        }

        public NewLine(string group, Fields fields)
        {
            this.group = group;
            this.fields = fields;
        }

    }

    public class SubmitFieldsRecord
    {

        public string type { get; set; }
        public string id { get; set; }
        public bool doSourcing { get; set; }
        public bool ignoreMandatoryFields { get; set; }
        public Fields fields { get; set; }

    }

    public class Fields
    {

        public List<Field> list { get; set; }

        public Fields(IEnumerable<Field> fields)
        {

            list = fields.ToList();

        }

        public Fields(object fields)
        {

            list = fields.GetType().GetProperties().Select(x => new Field(x.Name, x.GetValue(fields))).ToList();

        }

        public Fields(IEnumerable<(string, object)> fields)
        {

            list = fields.Select(x => new Field(x.Item1, x.Item2)).ToList();

        }

        public Fields(IEnumerable<KeyValuePair<string, object>> fields)
        {

            list = fields.Select(x => new Field(x.Key, x.Value)).ToList();

        }

        public Fields(Dictionary<string, object> fields)
        {

            list = fields.Select(x => new Field(x.Key, x.Value)).ToList();

        }

    }

    public class Field
    {
        public string name { get; set; }
        public object value { get; set; }

        public Field(string name, object value)
        {
            this.name = name;
            this.value = value;
        }

    }

    public class SearchColumn
    {
        public string name { get; set; }
        public string join { get; set; }
        public string summary { get; set; }
        public string formula { get; set; }
        public string function { get; set; }
        public string sort { get; set; }
        public string label { get; set; }

    }

    public class SearchFilter
    {
        public string name { get; set; }
        public string join { get; set; }
        public string @operator { get; set; }
        public object value1 { get; set; }
        public object value2 { get; set; }

    }

    public class NetsuiteTokenAuth
    {

        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string TokenID { get; set; }
        public string TokenSecret { get; set; }
        public string AccountID { get; set; }
        public Uri RestletUrl { get; set; }

        public static NetsuiteTokenAuth LoadFromEnvironment()
        {
            return new NetsuiteTokenAuth {

                ConsumerKey = Environment.GetEnvironmentVariable("NetsuiteConsumerKey"),
                ConsumerSecret = Environment.GetEnvironmentVariable("NetsuiteConsumerSecret"),
                TokenID = Environment.GetEnvironmentVariable("NetsuiteTokenID"),
                TokenSecret = Environment.GetEnvironmentVariable("NetsuiteTokenSecret"),
                AccountID = Environment.GetEnvironmentVariable("NetsuiteAccountID"),
                RestletUrl = new Uri(Environment.GetEnvironmentVariable("NetsuiteRestletUrl"))

            };
        }

    }

}
